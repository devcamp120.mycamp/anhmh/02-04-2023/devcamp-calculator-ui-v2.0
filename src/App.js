import { TfiArrowLeft } from "react-icons/tfi";
import './App.css';

function App() {
  return (
    <>
      <div className="container">
        <form>
          <input type="text" value=""></input>
        </form>

        <div className="keypad">
          <button id="del">Del</button>
          <button><TfiArrowLeft/></button>
          <button>/</button>
          <button>7</button>
          <button>8</button>
          <button>9</button>
          <button>*</button>
          <button>4</button>
          <button>5</button>
          <button>6</button>
          <button>-</button>
          <button>1</button>
          <button>2</button>
          <button>3</button>
          <button>+</button>
          <button>0</button>
          <button>,</button>
          <button id="result">=</button>
        </div>
      </div>
    </>
  );
}

export default App;
